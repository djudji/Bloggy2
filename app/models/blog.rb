class Blog < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  belongs_to :user
  acts_as_taggable
  has_many :votes, dependent: :destroy
  has_many :users, through: :votes

  # Enrols user to the course
  def voted(user)
    self.votes.where(user_id: [user.id]).any?
  end
end
