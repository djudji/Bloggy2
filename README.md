# Bloggy([Medium Clone](https://medium.com))

## Platforms and prerequistes
- Ruby 2.2.3
- Rails 4.2.4

## Features
- Multiple Users
- Blogging
- WSIYMG editor
- User Profiles
- Materialize.css framework
- Likes on Blogs
- Meta for search engines and sharing websites
- Taggings
- Amazon Web Services S3 or Google Cloud Storage
- Disqus Commentings
- Blog Searching
- Tag Searching

Created by [Sulman Baig](http://www.sulmanbaig.com).
Course to make this app is available at [CODElit](https://codelit.com)